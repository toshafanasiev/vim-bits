install: save
	@cp vimrc ~/.vimrc

save:
	@mv ~/.vimrc ~/.vimrc.$(shell /bin/date -Iminutes)

clean:
	@rm ~/.vimrc.*

clean-install: install clean

