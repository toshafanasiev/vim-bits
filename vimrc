set lines=48 columns=120
set number
set showmatch
set tabstop=4
set shiftwidth=4
set expandtab
set shiftround
set backspace=indent,eol,start
set cursorline
set autoindent
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
set laststatus=2
set encoding=utf-8
syntax on
"search
set hlsearch
set incsearch
set ignorecase
set smartcase
colors pablo

"no need for backup
set nobackup
set nowritebackup
set noswapfile

if has( "gui_running" )
  set guifont=Consolas:h10
  "set guioptions-=m "no menu bar
  set guioptions-=T "no tool bar
  set guioptions-=r "no right hand scroll bar
endif

if has("gui_gtk2")
    set guifont=Droid\ Sans\ Mono
elseif has("gui_macvim")
    set guifont=Consolas:h12
elseif has("gui_win32")
    set guifont=Consolas:h11
end

if &diff
  set columns=240
endif

function! PrettyXml()

python << EOF

import vim
from xml.dom import minidom

try:
  b = vim.current.buffer
  x = minidom.parseString( '\n'.join( b ) )
  b[:] = None
  for l in x.toprettyxml().split('\n'):
    b.append( str( l ) )
  del b[0] # remove empty first line
  
except Exception as ex:
  print ex

EOF

endfunction

command! -nargs=0 Pxml call PrettyXml()

au BufRead,BufNewFile *.fs set filetype=fsharp
au BufRead,BufNewFile *.fsx set filetype=fsharp
